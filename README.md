# MapleFone

This documentation will describe how-to make a MapleFone and the hardware and software it consists of. This phone is geared towards a general consumer with a lower ["threat model"](https://ssd.eff.org/en/glossary/threat-model).

The origin of the name comes from the love of New England maple syrup. If you're not sure why you might want a MapleFone, then please be aware that any non-modified phone running Google's Android has been known to give Google control over what apps your phone has installed. **This will not happen to a MapleFone user, as no Google Play Services are installed.**

## Let's get started

### 0 - To build your own or to buy one instead

* This documentation will explain how you can put together your own MapleFone. 
* Since I truly believe in DIY, encouraging people to "make" things, and transparency, this documentation includes everything to enable anyone in the world to make one.
* On a limited basis, I will be offering the MapleFone for people to purchase, so if interested please contact me and we can discuss.

I believe we are at a time when some people might seriously consider protecting themselves from the "clutches" of big ol' [Apple](https://www.nasdaq.com/market-activity/stocks/aapl) or the [G00g](https://www.nasdaq.com/market-activity/stocks/goog).

### 1 - Hardware and Operating System

In this section we discuss the hardware & the Operating System I have chosen for this phone called [LineageOS](https://lineageos.org).

I chose these for the following reasons:

* It's an open source version of Android
* It does not include any of the g00g's play services or store
* I do not set it up to run applications as root for [these reasons](https://madaidans-insecurities.github.io/android.html#rooting)
  * If you have not heard of Madaidan, then please take note that he knows his shhhht and enjoys sharing his findings :)

### 2 - Install LineageOS 

The LineageOS project Officially supports [tons of phones](https://download.lineageos.org/), but there are also phones which still have someone updating their own Unofficial versions too. 
I enjoy the Google Pixel phones (unlocked) as they're easy to work on, but your current phone might be able install it too. Some phones are very easy to install on (like Motorola Moto phones) but others are not easy (Samsung always makes it difficult!).

I then leave that up to the reader to figure out by web searching around. The [XDA Developers Forum](https://forum.xda-developers.com/) is a great place to search for phone info and has been for at least a decade. This is where you'd find unofficial builds of LineageOS if your phone was not Officially supported on the site. XDA has been around since 2002 and has been a trusted source for mobile phone development.

### 3 - Install those Apps

First off install [F-Droid store](https://f-droid.org) if it's not already installed as part of the above installation.

A MapleFone should only have applications that use the least amount of permissions.

These are all installable via the F-Droid store. Below take note that "E2EE" refers to ["End-to-End Encryption"](https://www.techtarget.com/searchsecurity/definition/end-to-end-encryption-E2EE). Here's the list:

| App name   | What it does                             |
|------------|:----------------------------------------:|
| Aegis Authenticator | Used for 2-Factor Authentication (if needed) |
| antennaPod | A free and secure podcasting application | 
| Element    | The [Matrix protocol phone app](https://en.wikipedia.org/wiki/Matrix_(protocol)) for the next generation of secure E2EE decentralized messaging | 
| InviZible Pro | An app to use TOR, I2P, or dnscrypt-proxy without needing root access | 
| Jitsi Meet | Instant video conferences efficiently adapting to your scale because we all need to start looking at this great positive alternative to the un-ethical company Zoom |
| Mull       | Privacy oriented web browser based upon Firefox |
| NewPipe    | An awesome safer YouTube app which (1) lets you download the video/music onto your phone from the app (2) can "subscribe" to your favorite streamers without having an account... (3) can stream other services, like PeerTube and SoundCloud | 
| Ning       | Simple and fast local network scanner. Similar to Fing. |
| Scrambled Exif | Remove the metadata from your pictures before sharing them |
| SecScanQR  | The QR code scanner/generator that cares about your privacy | 
| Fossify Calendar  | A free and open source private calendar app. | 
| Simple Clock      | The name says it all. Also this is better than the built-in clock app | 
| Simple Flashlight | The name says it all              | 
| Fossify Gallery | A great gallery application to replace the built-in gallery app. No Ads, Open-source, Private. No strings attached. |
| VLC        | The swiss-army knife of media players as this plays everything |

Optional recommendations "as needed" are the following apps too:
* Etesync - an app for a subscription-based (or self-hosted if your're a tech) secure E2EE service which is able to securely backup your contacts and calendar information
* JuiceSSH - if you need to SSH into a computer then this works pretty well for it
* Nextcloud apps - if you want to use a Nextcloud server, then these are available in the F-Droid store
* Stealth - an account-free, privacy-oriented, and feature-rich Reddit client
* Termux - if you need command line Linux things, then look into this.
* Monerujo - best phone Monero wallet, but only appears on F-Droid if you add a custom repository ([Instructions here](https://f-droid.monerujo.io/)).
* Fossify File Manager - I really like this FLOSS file manager on F-Droid. I removed it from list above as most users will be fine with the built-in file manager called "Files" :)
* SchildiChat - another Matrix client and fork of the Element client. Try this out if you're not a big fan of Element.
* Tusky - A [Mastodon](https://joinmastodon.org/) protocol phone app, which is a decentralized social media where no one is in charge!

If you do actually need something from the G00g's app store, then you can install [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/) from the F-Droid Store as a "safer" front-end which can be configured to use anonymous accounts created by the app's developer.

### 4 - Instruction guides

* [How to migrate contacts between phones](guides/contacts-migration.md)
* [How to Upgrade LineageOS on your MapleFone](guides/upgrade-lineageos.md)
* [How to get started with Element Matrix client](https://codeberg.org/jahway603/matrix-tldr-howto)

### Future plans

* To create a MapleFone variant with Linux running on it, like a [PinePhone](https://www.pine64.org/pinephone/).

### About Me

I have been modifying, customizing, and hacking Android phones since 2009. I hope you enjoy your phone whether you DIY or get one from me.

### Contact Info

If you know me personally, then just ask me about these phones.

If you are online only, then [find me on the Matrix](https://matrix.to/#/@jahway603:meowchat.xyz).

And please NO Signal Messenger because they implemented their own ishcoin behind everyone's back during most of last year, so no more trust for them from me and many others.

By visiting the [original page about it on Archive.org](https://web.archive.org/web/20201127142851/https://mobilecoin.foundation/about), as they have recently removed it from their site, you can see the following below. **I do not agree with this and have stopped using Signal Messenger after learning about it.** Everyone can make their own decision but I recommend to ditch Signal Messenger and to embrace the Matrix.

```
At MobileCoin, we believe governments have a legitimate interest in regulating the economic lives of their citizens.
```

If you want to learn more about how Signal Messenger has betrayed all of us, then please [review this video](https://www.bitchute.com/video/tJoO2uWrX1M/). If you want to learn more about why Signal Messenger is bad news, then I recommend [reading these articles](https://codeberg.org/jahway603/yno/src/branch/master/yno-signal.md).

#### License

[GPLv3](LICENSE)
