# How to Upgrade LineageOS on your MapleFone
This write-up will present instructions on how-to upgrade the current running Operating System (OS), which is called LineageOS, on your MapleFone.

## Migration Steps
Steps to be performed on your MapleFone will be presented below. Please follow the pre-steps first to determine which instructions you should follow next in this process.

### Pre-Steps to determine next steps to follow
1. In your MapleFone's Apps list, select Settings.
1. Then select "About phone".
1. Then take note of both the "Android version" and the "LineageOS version" here.
    - If you are running "Android version" of 14 and "LineageOS version" of 21.x, then follow the "Soft upgrade steps" below.
    - If you are running "Android version" of 13 and "LineageOS version" of 20.x, then follow the "Involved upgrade steps" below.
    - If you are running "Android version" of 12 and "LineageOS version" of 19.x, then follow the "Involved upgrade steps" below.

### Soft upgrade steps (refer to pre-steps)
1. In your MapleFone's Apps list, select Settings.
1. Navigate to "System" and then to "Updater".
1. Click the Refresh Icon in the top right corner.
1. Choose which update you’d like and press “Download”.
1. When the download completes, click “Install”. Once the update process has finished, the device will display a “Reboot” button, you may need to go into the Updater menu in Settings, “System” to see it. This will reboot you into the updated system.

### Involved upgrade steps (refer to pre-steps)
1. Disclaimer: If you are not technical, then you may not want to follow this involved upgrade process and instead opt to get a technical friend to do it for you instead.
1. Since the Updater app does not support upgrading from one version of LineageOS to a newer version, this process will be more involved.
1. Download the LineageOS install package for your MapleFone version off of https://wiki.lineageos.org/devices/
1. Make sure `adb` is working correctly on your computer.
1. Enable USB debugging on your device.
1. Run `adb -d reboot sideload`, which will reboot the phone.
1. Now run `adb -d sideload /path/to/zip` with the correct path to your downloaded LineageOS package.
1. Once you have installed everything successfully, click the back arrow in the top left of the screen, then “Reboot system now”.

## Success!
Now your new MapleFone should be running the latest version of LineageOS Android.    
Please let me know if you need any help with this process. If this process is too technical, then I can also be hired to complete this for you.

### Credits
jahway603    
Last Updated March 27, 2024
