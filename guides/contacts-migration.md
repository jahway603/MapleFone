# How to migrate contacts between phones
This write-up will present instructions on how-to backup and restore (or migrate) your contacts between phones. There are other methods of migrating contacts between phones, so this will only be documenting my recommended method.

## Migration Steps
Steps to be performed on your old Android phone and your new Android phone will be presented below.

### Steps performed on your Old Android Phone
1. On your Old Android phone, you want to navigate to your Contacts app.
1. In the Contacts app on your Old Android phone, you want to export all of your contacts there to a "Virtual Contact File", or VCF file. [Instructions to do this can be found here](https://www.wikihow.com/Export-Contacts-on-Android).
1. Take note of where your phone stores this VCF file as it will be needed for the next steps.
1. On your Old Android phone, install the EteSync application, which can be [found here on the F-Droid Store](https://f-droid.org/en/packages/com.etesync.syncadapter/).
1. Open EteSync and click on the Blue plus icon in the bottom right hand corner.

    ![](images/granitesync-main.png)

1. Next, enter your GraniteSync username, password, and the GraniteSync server. You will need to check the box "Show advanced settings" to enter the GraniteSync server as shown in the image.

    ![](images/granitesync-self-host.png)

1. Click on "LOG IN" in the bottom right hand corner.
1. Next, click on your GraniteSync Account on the next screen and you will be presented with an Account Screen similar to the following image.

    ![](images/granitesync-account-page.png)

1. Next, click on the first entry listed under Contacts on this Account Screen.
1. You will be presented with an almost blank screen.
1. Click on the three dots icon in the top right hand corner, as seen in this image.

    ![](images/granitesync-view-collection.png)

1. Then click on Import.
1. Next, click on "From File" and select the location of your VCF file to import your contacts into your GraniteSync Account.
1. Now move to the steps below to perform on your New Android phone.

### Steps performed on your New Android Phone
1. On your New Android phone, you will repeat the above steps to install EteSync and to login to your GraniteSync Account.
1. After doing so, all your Contacts should now appear in your New Phone's Contacts app.

## Success!
Now your new Android phone should have all your contacts from your old Android phone.
Please let me know if you need any help with this process.

### Credits
jahway603
Last Updated February 13, 2024
